<?php

namespace App\Entity;

use App\Model\Client\ClientHandler;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Clent\ClentInterface;

/**
 * @ORM\Entity
 */
class Cottage extends BookingObject
{
    /**
     * @ORM\Column(type="boolean", length=128)
     */
    private $Kitchen;

    /**
     * @param mixed $Kitchen
     * @return Cottage
     */
    public function setKitchen($Kitchen)
    {
        $this->Kitchen = $Kitchen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->Kitchen;
    }
}
