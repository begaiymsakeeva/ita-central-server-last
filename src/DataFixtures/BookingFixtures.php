<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BookingFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $bookingObject1 = new Cottage();
        $bookingObject1->setName('Местная забегаловка');
        $bookingObject1->setKitchen(true);

        $bookingObject2 = new Pension();
        $bookingObject2->setName('Неместная забегаловка');
        $bookingObject2->setCook(true);

        $manager->persist($bookingObject1);
        $manager->persist($bookingObject2);

        $manager->flush();
    }
}
